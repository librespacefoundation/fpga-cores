# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "C_S00_AXI_CFG_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_CFG_HIGHADDR" -parent ${Page_0}

  set ZERO_PREAMBLE_LEN [ipgui::add_param $IPINST -name "ZERO_PREAMBLE_LEN"]
  set_property tooltip {Set the number of zero samples that the core should transmit before the start of the first sample} ${ZERO_PREAMBLE_LEN}

}

proc update_PARAM_VALUE.ZERO_PREAMBLE_LEN { PARAM_VALUE.ZERO_PREAMBLE_LEN } {
	# Procedure called to update ZERO_PREAMBLE_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ZERO_PREAMBLE_LEN { PARAM_VALUE.ZERO_PREAMBLE_LEN } {
	# Procedure called to validate ZERO_PREAMBLE_LEN
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_CFG_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_CFG_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_CFG_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_CFG_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_BASEADDR { PARAM_VALUE.C_S00_AXI_CFG_BASEADDR } {
	# Procedure called to update C_S00_AXI_CFG_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_BASEADDR { PARAM_VALUE.C_S00_AXI_CFG_BASEADDR } {
	# Procedure called to validate C_S00_AXI_CFG_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR { PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR } {
	# Procedure called to update C_S00_AXI_CFG_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR { PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_CFG_HIGHADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH { PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to update C_S00_AXIS_IQ_TDATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH { PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to validate C_S00_AXIS_IQ_TDATA_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH { MODELPARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXIS_IQ_TDATA_WIDTH}
}

proc update_MODELPARAM_VALUE.ZERO_PREAMBLE_LEN { MODELPARAM_VALUE.ZERO_PREAMBLE_LEN PARAM_VALUE.ZERO_PREAMBLE_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ZERO_PREAMBLE_LEN}] ${MODELPARAM_VALUE.ZERO_PREAMBLE_LEN}
}

