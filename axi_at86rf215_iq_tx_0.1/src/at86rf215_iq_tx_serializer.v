`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/03/2020 05:44:58 PM
// Design Name: 
// Module Name: at86rf215_iq_tx_serializer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module at86rf215_iq_tx_serializer #
(
        /*  Datasheet specifies that at least 32 samples are zero at the start of every transmission */
        parameter unsigned ZERO_PREAMBLE_LEN = 48
)
(
    input iq_clk,
    input resetn,
    input [12:0] idata,
    input [12:0] qdata,
    input [3:0] samp_rate,
    input mark_sync, 
    output reg txd,
    output reg next_sample_req,
    output reg underflow
);

    function integer clogb2 (input integer bit_depth); begin                                                                              
        for(clogb2=0; bit_depth>0; clogb2=clogb2+1)                                      
            bit_depth = bit_depth >> 1;                                                    
        end                                                                                
    endfunction
    
    reg [clogb2(32*10 + ZERO_PREAMBLE_LEN):0] sample_cnt;
    reg [clogb2(32*10):0] zero_pad;
    reg [15:0] idata_full;
    reg [15:0] qdata_full;
    reg init;
    reg sync;
    
    always @(sample_cnt) begin
        if(init) begin
            if(sample_cnt == ZERO_PREAMBLE_LEN - 1) begin
                idata_full <= {2'b10, idata, sync};
                qdata_full <= {2'b01, qdata, 1'b0};
            end
        end
        else begin
            if(sample_cnt == 32 + zero_pad - 1) begin
                idata_full <= {2'b10, idata, sync};
                qdata_full <= {2'b01, qdata, 1'b0};
            end
        end
    end
    
    always @(posedge iq_clk) begin
        if(!resetn) begin
            next_sample_req <= 1'b0;
        end
        else begin
            if(init) begin
                if(sample_cnt == ZERO_PREAMBLE_LEN - 1) begin
                    next_sample_req <= 1;
                end
                else if(sample_cnt == ZERO_PREAMBLE_LEN + 32 + zero_pad - 1) begin
                    next_sample_req <= 1;
                    init <= 0;
                end
                else begin
                    next_sample_req <= 0;
                end
            end
            else begin
                if(sample_cnt == 32 + zero_pad - 1) begin
                    next_sample_req <= 1;
                end
                else begin
                    next_sample_req <= 0;
                end
            end
        end
    end
    
    always @(posedge iq_clk) begin
        if (!resetn) begin
            txd <= 1'b0;
            /* Sampling rate and SYNC marking can change only during reset */
            zero_pad <= (samp_rate - 1) * 32;
            init <= 1;
            sample_cnt <= 0;
            sync <= mark_sync;
        end
        else begin
            if(init) begin
                if(sample_cnt < ZERO_PREAMBLE_LEN) begin
                    txd <= 1'b0;
                end
                else if (sample_cnt < ZERO_PREAMBLE_LEN + 16) begin
                    txd <= idata_full[15 - (sample_cnt - ZERO_PREAMBLE_LEN)];
                end
                else if (sample_cnt < ZERO_PREAMBLE_LEN + 32) begin
                    txd <= qdata_full[31 - (sample_cnt - ZERO_PREAMBLE_LEN)];
                end
                else begin
                    txd <= 1'b0;
                end
                
                if(sample_cnt < ZERO_PREAMBLE_LEN + 32 + zero_pad - 1) begin
                    sample_cnt <= sample_cnt + 1;
                end
                else begin 
                    sample_cnt <= 0;
                end
            end
            else begin
                if (sample_cnt < 16) begin
                    txd <= idata_full[15 - sample_cnt];
                end
                else if (sample_cnt < 32) begin
                    txd <= qdata_full[31 - sample_cnt];
                end
                else begin
                    txd <= 1'b0;
                end
                
                if(sample_cnt < 32 + zero_pad - 1) begin
                    sample_cnt <= sample_cnt + 1;    
                end
                else begin 
                    sample_cnt <= 0;
                end
            end
        end 
    end
endmodule
