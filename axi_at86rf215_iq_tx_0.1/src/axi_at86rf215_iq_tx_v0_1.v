
`timescale 1 ns / 1 ps

	module axi_at86rf215_iq_tx_v0_1 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS_IQ
		parameter integer C_S00_AXIS_IQ_TDATA_WIDTH	= 32,

		// Parameters of Axi Slave Bus Interface S00_AXI_CFG
		parameter integer C_S00_AXI_CFG_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_CFG_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
        output wire txclk,
        output wire txd,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXIS_IQ
		input wire  s00_axis_iq_aclk,
		input wire  s00_axis_iq_aresetn,
		output wire  s00_axis_iq_tready,
		input wire [C_S00_AXIS_IQ_TDATA_WIDTH-1 : 0] s00_axis_iq_tdata,
		input wire [(C_S00_AXIS_IQ_TDATA_WIDTH/8)-1 : 0] s00_axis_iq_tstrb,
		input wire  s00_axis_iq_tlast,
		input wire  s00_axis_iq_tvalid,

		// Ports of Axi Slave Bus Interface S00_AXI_CFG
		input wire  s00_axi_cfg_aclk,
		input wire  s00_axi_cfg_aresetn,
		input wire [C_S00_AXI_CFG_ADDR_WIDTH-1 : 0] s00_axi_cfg_awaddr,
		input wire [2 : 0] s00_axi_cfg_awprot,
		input wire  s00_axi_cfg_awvalid,
		output wire  s00_axi_cfg_awready,
		input wire [C_S00_AXI_CFG_DATA_WIDTH-1 : 0] s00_axi_cfg_wdata,
		input wire [(C_S00_AXI_CFG_DATA_WIDTH/8)-1 : 0] s00_axi_cfg_wstrb,
		input wire  s00_axi_cfg_wvalid,
		output wire  s00_axi_cfg_wready,
		output wire [1 : 0] s00_axi_cfg_bresp,
		output wire  s00_axi_cfg_bvalid,
		input wire  s00_axi_cfg_bready,
		input wire [C_S00_AXI_CFG_ADDR_WIDTH-1 : 0] s00_axi_cfg_araddr,
		input wire [2 : 0] s00_axi_cfg_arprot,
		input wire  s00_axi_cfg_arvalid,
		output wire  s00_axi_cfg_arready,
		output wire [C_S00_AXI_CFG_DATA_WIDTH-1 : 0] s00_axi_cfg_rdata,
		output wire [1 : 0] s00_axi_cfg_rresp,
		output wire  s00_axi_cfg_rvalid,
		input wire  s00_axi_cfg_rready
	);
// Instantiation of Axi Bus Interface S00_AXIS_IQ
	axi_at86rf215_iq_tx_v0_1_S00_AXIS_IQ # ( 
		.C_S_AXIS_TDATA_WIDTH(C_S00_AXIS_IQ_TDATA_WIDTH)
	) axi_at86rf215_iq_tx_v0_1_S00_AXIS_IQ_inst (
		.S_AXIS_ACLK(s00_axis_iq_aclk),
		.S_AXIS_ARESETN(s00_axis_iq_aresetn),
		.S_AXIS_TREADY(s00_axis_iq_tready),
		.S_AXIS_TDATA(s00_axis_iq_tdata),
		.S_AXIS_TSTRB(s00_axis_iq_tstrb),
		.S_AXIS_TLAST(s00_axis_iq_tlast),
		.S_AXIS_TVALID(s00_axis_iq_tvalid)
	);

// Instantiation of Axi Bus Interface S00_AXI_CFG
	axi_at86rf215_iq_tx_v0_1_S00_AXI_CFG # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_CFG_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_CFG_ADDR_WIDTH)
	) axi_at86rf215_iq_tx_v0_1_S00_AXI_CFG_inst (
		.S_AXI_ACLK(s00_axi_cfg_aclk),
		.S_AXI_ARESETN(s00_axi_cfg_aresetn),
		.S_AXI_AWADDR(s00_axi_cfg_awaddr),
		.S_AXI_AWPROT(s00_axi_cfg_awprot),
		.S_AXI_AWVALID(s00_axi_cfg_awvalid),
		.S_AXI_AWREADY(s00_axi_cfg_awready),
		.S_AXI_WDATA(s00_axi_cfg_wdata),
		.S_AXI_WSTRB(s00_axi_cfg_wstrb),
		.S_AXI_WVALID(s00_axi_cfg_wvalid),
		.S_AXI_WREADY(s00_axi_cfg_wready),
		.S_AXI_BRESP(s00_axi_cfg_bresp),
		.S_AXI_BVALID(s00_axi_cfg_bvalid),
		.S_AXI_BREADY(s00_axi_cfg_bready),
		.S_AXI_ARADDR(s00_axi_cfg_araddr),
		.S_AXI_ARPROT(s00_axi_cfg_arprot),
		.S_AXI_ARVALID(s00_axi_cfg_arvalid),
		.S_AXI_ARREADY(s00_axi_cfg_arready),
		.S_AXI_RDATA(s00_axi_cfg_rdata),
		.S_AXI_RRESP(s00_axi_cfg_rresp),
		.S_AXI_RVALID(s00_axi_cfg_rvalid),
		.S_AXI_RREADY(s00_axi_cfg_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
