`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2020 02:31:21 PM
// Design Name: 
// Module Name: axi_at86rf215_iq_tx_serializer_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axi_at86rf215_iq_tx_serializer_tb(

    );
    

    parameter CLK_PERIOD = 8;
    reg iq_clk;
    reg resetn;
    reg [3:0] samp_rate;
    reg mark_sync;
    reg signed [12:0] idata;
    reg signed [12:0] qdata;
    wire txd;
    wire sample_req;
    
    at86rf215_iq_tx_serializer # (.ZERO_PREAMBLE_LEN(4)) 
        dut(.iq_clk(iq_clk),
            .resetn(resetn),
            .idata(idata),
            .qdata(qdata),
            .samp_rate(samp_rate),
            .mark_sync(mark_sync),
            .txd(txd),
            .sample_req(sample_req));
    
    initial begin
        iq_clk = 1;
        samp_rate = 1;
        resetn = 0;
        idata = 0;
        qdata = 0;
        mark_sync = 0;
        #(10*CLK_PERIOD) resetn = 1;
        #(1500*CLK_PERIOD) resetn = 0;
        $finish;
    end
    
    always #(CLK_PERIOD/2) iq_clk = ~iq_clk;
    
    always @(posedge iq_clk) begin
        if(sample_req) begin
            idata <= 0;
            qdata <= 0;
        end
    end

endmodule
