`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/03/2020 05:44:58 PM
// Design Name: 
// Module Name: at86rf215_iq_tx_serializer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module at86rf215_iq_tx_serializer # 
(
    parameter unsigned ZERO_PREAMBLE_LEN = 32
)
(
    input iq_clk,
    input resetn,
    input samp_valid,
    input [12:0] idata,
    input [12:0] qdata,
    input [3:0] samp_rate,
    input mark_sync, 
    output reg txd,
    output reg sample_req,
    output reg underflow
);

function integer clogb2 (input integer bit_depth);                                   
  begin                                                                              
    for(clogb2=0; bit_depth>0; clogb2=clogb2+1)                                      
      bit_depth = bit_depth >> 1;                                                    
  end                                                                                
endfunction

reg [29:0] word;
reg [clogb2(32*10):0] zero_pad;
reg [clogb2(32*10 + ZERO_PREAMBLE_LEN):0] bit_cnt;
reg init;
reg mark;

always @(posedge iq_clk) begin
    if(!resetn) begin
        txd <= 0;
    end
    else begin
        if(init) begin
            if(bit_cnt < ZERO_PREAMBLE_LEN) begin
                txd <= 0;
            end        
            /* First I sync marker*/    
            else if(bit_cnt == ZERO_PREAMBLE_LEN) begin
                txd <= 1;
            end
            /* Second I sync marker*/    
            else if(bit_cnt == ZERO_PREAMBLE_LEN + 1) begin
                txd <= 0;
            end
            else if(bit_cnt < ZERO_PREAMBLE_LEN + 32) begin
                txd <= word[29 - (bit_cnt - 2 -ZERO_PREAMBLE_LEN)]; 
            end
            else begin
                txd <= 0;
            end
        end
        else begin
            /* First I sync marker*/    
            if(bit_cnt == 0) begin
                txd <= 1;
            end
            /* Second I sync marker*/    
            else if(bit_cnt == 1) begin
                txd <= 0;
            end
            else if(bit_cnt < 32) begin
                txd <= word[29 - (bit_cnt - 2)];
            end
            else begin
                txd <= 0;
             end
        end
    end
end

always @(posedge iq_clk) begin
    if(!resetn) begin
        sample_req <= 0;
        bit_cnt <= 0;
        word <= 0;
        mark <= mark_sync;
        zero_pad <= (samp_rate - 1) * 32;
        init <= 1;
    end
    else begin
        if(init) begin
            if(bit_cnt == ZERO_PREAMBLE_LEN) begin
                sample_req <= 1;
                bit_cnt <= bit_cnt + 1;
            end
            else if(bit_cnt == ZERO_PREAMBLE_LEN + 1) begin
                bit_cnt <= bit_cnt + 1;
                sample_req <= 0;
            end
            else if(bit_cnt == ZERO_PREAMBLE_LEN + 2) begin
                bit_cnt <= bit_cnt + 1;
                word <= {idata, mark, 2'b01, qdata, 1'b0};
            end
            else if(bit_cnt == ZERO_PREAMBLE_LEN + 32 + zero_pad - 1) begin
                bit_cnt <= 0;
                init <= 0;
            end
            else begin
                bit_cnt <= bit_cnt + 1;
            end
        end
        else begin
            if(bit_cnt == 0) begin
                sample_req <= 1;
                bit_cnt <= bit_cnt + 1;
            end
            else if(bit_cnt == 1) begin
                sample_req <= 0;
                bit_cnt <= bit_cnt + 1;
            end
            else if(bit_cnt == 2) begin
                word <= {idata, mark, 2'b01, qdata, 1'b0};
                bit_cnt <= bit_cnt + 1; 
            end
            else if(bit_cnt == 32 + zero_pad - 1) begin
                bit_cnt <= 0;
            end
            else begin
                bit_cnt <= bit_cnt + 1;
            end
        end
    end
end

endmodule
