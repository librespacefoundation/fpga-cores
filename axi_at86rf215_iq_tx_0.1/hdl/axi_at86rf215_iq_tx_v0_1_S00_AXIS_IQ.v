
`timescale 1 ns / 1 ps

	module axi_at86rf215_iq_tx_v0_1_S00_AXIS_IQ #
	(
		parameter integer C_S_AXIS_TDATA_WIDTH	= 32,
		parameter integer ZERO_PREAMBLE_LEN = 32
	)
	(
		// Users to add ports here
        input wire [3:0] samp_rate,
        input wire mark_sync,
        output reg txd,
        output reg underflow,
		// User ports ends
		// Do not modify the ports beyond this line

		// AXI4Stream sink: Clock
		input wire  S_AXIS_ACLK,
		// AXI4Stream sink: Reset
		input wire  S_AXIS_ARESETN,
		// Ready to accept data in
		output wire  S_AXIS_TREADY,
		// Data in
		input wire [C_S_AXIS_TDATA_WIDTH-1 : 0] S_AXIS_TDATA,
		// Byte qualifier
		input wire [(C_S_AXIS_TDATA_WIDTH/8)-1 : 0] S_AXIS_TSTRB,
		// Indicates boundary of last packet
		input wire  S_AXIS_TLAST,
		// Data is in valid
		input wire  S_AXIS_TVALID
	);

    function integer clogb2 (input integer bit_depth);                                   
      begin                                                                              
        for(clogb2=0; bit_depth>0; clogb2=clogb2+1)                                      
          bit_depth = bit_depth >> 1;                                                    
      end                                                                                
    endfunction
    
    reg [31:0] words[1:0];
    
    reg [clogb2(32*10):0] zero_pad;
    reg [clogb2(32*10 + ZERO_PREAMBLE_LEN):0] bit_cnt;
    reg init;
    reg mark;
    

	parameter [1:0] IDLE = 1'b0,           // This is the initial/idle state 
	                ACCEPT_SAMPLE  = 1'b1; // Accept a sample from the stream 
	wire  	axis_tready;
	// State variable
	reg mst_exec_state;       
	// FIFO write enable
	wire fifo_wren;
	reg [1:0] word_cnt;
	reg read_idx;
	reg write_idx;

	assign S_AXIS_TREADY	= axis_tready;
	// Control state machine implementation
	always @(posedge S_AXIS_ACLK) 
	begin  
	  if (!S_AXIS_ARESETN) begin
	      mst_exec_state <= IDLE;
	  end  
	  else
	    case (mst_exec_state)
	      IDLE: begin
	          if (S_AXIS_TVALID && word_cnt < 2) begin
	              mst_exec_state <= ACCEPT_SAMPLE;
	          end
	          else begin
	              mst_exec_state <= IDLE;
	          end
	      end
	      ACCEPT_SAMPLE: 
	        // When the sink has accepted all the streaming input data,
	        // the interface swiches functionality to a streaming master
	        if (word_cnt > 1) begin
	            mst_exec_state <= IDLE;
	          end
	        else begin
	            mst_exec_state <= ACCEPT_SAMPLE;
	          end
	    endcase
	end


	assign axis_tready = ((mst_exec_state == ACCEPT_SAMPLE));
    assign fifo_wren = S_AXIS_TVALID && axis_tready;

    always @(posedge S_AXIS_ACLK) begin
        if(!S_AXIS_ARESETN) begin
            mark <= mark_sync;
            zero_pad <= (samp_rate - 1) * 32;
            word_cnt <= 0;
            words[0] <= 0;
            words[1] <= 0;
            read_idx <= 0;
            write_idx <= 0;
        end
        else begin
            if(fifo_wren) begin
                words[write_idx] <= {2'b10, S_AXIS_TDATA[16+13-1:16], mark, 2'b01, S_AXIS_TDATA[12:0], 1'b0};
                write_idx <= write_idx + 1;
                word_cnt <= word_cnt + 1;
            end
            
            if(bit_cnt == 32 + zero_pad - 1) begin
                word_cnt <= word_cnt - 1;
                read_idx <= read_idx + 1;
            end
        end
    end
    
    always @(posedge S_AXIS_ACLK) begin
        if(!S_AXIS_ARESETN) begin
            txd <= 0;
            underflow <= 0;
            bit_cnt <= 0;
            init <= 1;
        end
        else begin
            /* 
             * At the start of a new IQ session the IC needs at least
             * 32 zero bits
             */
            if(init) begin
                txd <= 0;
                bit_cnt <= bit_cnt + 1;
                if(bit_cnt >= ZERO_PREAMBLE_LEN - 1) begin
                    init <= 0;
                    bit_cnt <= 0;
                end 
            end
            else begin
                if(word_cnt == 0) begin
                    underflow <= 1;
                    txd <= 0;
                end
                else begin
                    underflow <= 0;
                    if(bit_cnt < 32) begin
                        txd <= words[read_idx][31-bit_cnt];
                    end
                    else begin
                        txd <= 0;
                    end
                    bit_cnt <= bit_cnt + 1;
                    if(bit_cnt == 32 + zero_pad - 1) begin
                        bit_cnt <= 0;
                    end
                end
            end
        end
    end
    
	endmodule
