`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2020 05:53:47 PM
// Design Name: 
// Module Name: at86rf215_iq_rx_sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module at86rf215_iq_rx_sync # 
(
    parameter integer SYNC_THRESHOLD = 5
)
(
    input iq_clk,
    input iq_data,
    input [3:0] samp_rate,
    input resetn,
    output reg [12:0] idata,
    output reg [12:0] qdata,
    output reg valid,
    output reg in_sync
);
    
    function integer clogb2 (input integer bit_depth);                                   
	  begin                                                                              
	    for(clogb2=0; bit_depth>0; clogb2=clogb2+1)                                      
	      bit_depth = bit_depth >> 1;                                                    
	  end                                                                                
	endfunction
	
	localparam integer CNT_BITS = clogb2(SYNC_THRESHOLD);
    
    wire [31:0] sreg;
    reg [31:0] sreg_p;
    reg [31:0] sreg_n;
    // This counter will count at most 10 32-bit words
    reg [clogb2(10*32)-1:0] bit_cnt;
    reg [clogb2(10*32)-1:0] bit_thresh;
    reg [clogb2(SYNC_THRESHOLD)-1:0] sync_cnt;
     
    assign sreg = sreg_p | sreg_n;
    /*
     * Shift the bit stream inside this 32-bit register so the module can sync 
     * on the special markers of the 32-bit word
     * 
     * The IQ stream of the AT86RF215 is clocked on both edges of the clock.
     * This requires dual edge flip-flops, but for compatibility reasons we
     * are implementing using two separate registers   
     */
    always @(posedge iq_clk) begin
        if(!resetn) begin
            sreg_p <= 0;
        end 
        else begin
            sreg_p <= sreg_p << 2;
            sreg_p[1] <= iq_data;
        end 
    end
    
    always @(negedge iq_clk) begin
        if(!resetn) begin
            sreg_n <= 0;
        end 
        else begin
            sreg_n <= sreg_n << 2;
            sreg_n[0] <= iq_data;
        end 
    end
    
    /*
     * Sampling rate setting can change only during reset
     */
    always @(posedge iq_clk) begin
        if(!resetn) begin
             if(samp_rate == 2) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 3) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 4) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 5) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 6) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 8) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else if (samp_rate == 10) begin
                bit_thresh <= samp_rate * 16 - 1;
             end
             else begin
                bit_thresh <= 31;
             end
        end
    end
    
    always @(posedge iq_clk) begin
        if(!resetn) begin
            in_sync <= 0;
            bit_cnt <= 0;
            sync_cnt <= 0;
            valid <= 0;
        end 
        else begin
            // Try to find SYNC_THRESHOLD valid 32-bit words from the AT86RF215
            if(!in_sync) begin
                // Try to find the first one
                if(sync_cnt == 0) begin
                    bit_cnt <= 0;
                    if(sreg[31:30] == 2'b10 && sreg[15:14] == 2'b01) begin
                        if(SYNC_THRESHOLD == 1) begin
                            in_sync <= 1;
                        end
                        else begin
                            in_sync <= 0;
                        end
                        sync_cnt <= 1;
                    end
                end
                else begin
                    if(bit_cnt == 15) begin
                        if(sreg[31:30] == 2'b10 && sreg[15:14] == 2'b01) begin
                            if(sync_cnt >= SYNC_THRESHOLD - 1) begin
                                in_sync <= 1;
                            end
                            else begin
                                in_sync <= 0;
                            end
                            sync_cnt <= sync_cnt + 1;
                        end
                        bit_cnt <= 0;
                    end
                    else begin
                        bit_cnt <= bit_cnt + 1;
                    end
                end
                
            end
            else begin
                if(bit_cnt == bit_thresh) begin
                    if(sreg[31:30] == 2'b10 && sreg[15:14] == 2'b01) begin
                        idata <= sreg[29:17];
                        qdata <= sreg[13:1];
                        valid <= 1'b1;
                        in_sync <= 1;
                    end
                    else begin
                        in_sync <= 0;
                        valid <= 1'b0;
                        sync_cnt <= 0;
                    end
                    bit_cnt <= 0;
                end
                else begin
                    valid <= 1'b0;
                    bit_cnt <= bit_cnt + 1;
                end       
            end
        end 
    end
endmodule
