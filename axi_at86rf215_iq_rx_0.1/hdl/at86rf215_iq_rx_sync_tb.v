`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2020 06:53:23 PM
// Design Name: 
// Module Name: at86rf215_iq_rx_sync_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module at86rf215_iq_rx_sync_tb(

    );
    
    parameter CLK_PERIOD = 8;
    reg iq_clk;
    reg iq_data;
    reg resetn;
    wire signed [12:0] idata;
    wire signed [12:0] qdata;
    wire valid;
    wire in_sync;
    reg [4:0] cnt;
    reg inject_error;
    reg [3:0] samp_rate;
    
    reg signed [31:0] lvds_sample = {2'b10, 14'b0, 2'b01, 14'b0};
    
    at86rf215_iq_rx_sync # (.SYNC_THRESHOLD(3))
        dut(.iq_clk(iq_clk),
            .iq_data(iq_data),
            .samp_rate(samp_rate),
            .resetn(resetn),
            .idata(idata),
            .qdata(qdata),
            .valid(valid),
            .in_sync(in_sync));
    
    initial begin
        inject_error = 0;
        cnt = 0;
        iq_clk = 1;
        samp_rate = 10;
        resetn = 0;
        iq_data = 0;
        #(10*CLK_PERIOD) resetn = 1;
        #(180*CLK_PERIOD) inject_error = 1;
        #(80*CLK_PERIOD) inject_error = 0;
        #(1500*CLK_PERIOD) resetn = 0;
        $finish;
    end
    
    always #(CLK_PERIOD/2) iq_clk = ~iq_clk;
    
    always @(iq_clk) begin
        if(!resetn) begin
            cnt <= 0;
        end
        else begin 
            cnt <= cnt + 1;
        end
    end
    always @(iq_clk) begin
        if(resetn) begin
            iq_data <= lvds_sample[31 - cnt];
        end
    end
    
    always @(iq_clk) begin
        if(inject_error) begin
            lvds_sample <= 0;
        end
        else begin
            if(cnt == 31) begin
                lvds_sample <= {2'b10, lvds_sample[29:17] + 1'b1, 1'b0, 2'b01, lvds_sample[13:1] + 1'b1, 1'b0};
            end
        end
    end
endmodule
