`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 11/18/2020 07:08:33 PM
// Design Name:
// Module Name: axi_at86rf215_iq_rx_M_AXI_S
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module axi_at86rf215_iq_rx_M_AXI_S #
(
        parameter integer C_M_AXIS_TDATA_WIDTH	= 16
)
(
        // Users to add ports here
        input wire [12:0] sample,
        input wire new_sample,
        output reg overflow,
        // User ports ends
        // Do not modify the ports beyond this line

        // Global ports
        input wire  M_AXIS_ACLK,
        //
        input wire  M_AXIS_ARESETN,
        // Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted.
        output wire  M_AXIS_TVALID,
        // TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
        output wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA,
        // TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
        output wire [(C_M_AXIS_TDATA_WIDTH/8)-1 : 0] M_AXIS_TSTRB,
        // TLAST indicates the boundary of a packet.
        output wire  M_AXIS_TLAST,
        // TREADY indicates that the slave can accept a transfer in the current cycle.
        input wire  M_AXIS_TREADY
);

    /* Ring  buffer holding at most two samples */
    reg [12:0] samples[1:0];
    reg [1:0] samples_cnt;
    reg read_idx;
    reg write_idx;

    // Define the states of state machine
    // The control state machine oversees the writing of input streaming data to the FIFO,
    // and outputs the streaming data from the FIFO
    parameter IDLE = 1'b0,        // This is the initial/idle state
            SEND_STREAM = 1'b1; // In this state the
                                // stream data is output through M_AXIS_TDATA
    // State variable
    reg mst_exec_state;

    //streaming data valid
    wire  	axis_tvalid;
    //streaming data valid delayed by one clock cycle
    reg  	axis_tvalid_delay;
    //Last of the streaming data
    wire  	axis_tlast;
    //Last of the streaming data delayed by one clock cycle
    reg  	axis_tlast_delay;
    //FIFO implementation signals
    reg [C_M_AXIS_TDATA_WIDTH-1 : 0] 	stream_data_out;
    wire  	tx_en;


    // I/O Connections assignments

    assign M_AXIS_TVALID	= axis_tvalid_delay;
    assign M_AXIS_TDATA	= stream_data_out;
    assign M_AXIS_TLAST	= axis_tlast_delay;
    assign M_AXIS_TSTRB	= {(C_M_AXIS_TDATA_WIDTH/8){1'b1}};


    // Control state machine implementation
    always @(posedge M_AXIS_ACLK) begin
        if (!M_AXIS_ARESETN) begin
            mst_exec_state <= IDLE;
        end
        else begin
            case (mst_exec_state)
                IDLE:
                if (samples_cnt > 0) begin
                    mst_exec_state  <= SEND_STREAM;
                end
                else begin
                    mst_exec_state  <= IDLE;
                end

                SEND_STREAM:
                if (samples_cnt == 0) begin
                    mst_exec_state <= IDLE;
                end
                else begin
                    mst_exec_state <= SEND_STREAM;
                end
            endcase
        end
    end


    //tvalid generation
    //axis_tvalid is asserted when the control state machine's state is SEND_STREAM
    assign axis_tvalid = ((mst_exec_state == SEND_STREAM) && samples_cnt > 0);
    assign tx_en = M_AXIS_TREADY && axis_tvalid;

    // Delay the axis_tvalid and axis_tlast signal by one clock cycle
    // to match the latency of M_AXIS_TDATA
    always @(posedge M_AXIS_ACLK) begin
        if (!M_AXIS_ARESETN) begin
            axis_tvalid_delay <= 1'b0;
            axis_tlast_delay <= 1'b0;
        end
        else begin
            if(axis_tvalid_delay == 1'b1) begin
                if(M_AXIS_TREADY == 1'b1) begin
                    axis_tvalid_delay <= axis_tvalid;
                end
                else begin
                    axis_tvalid_delay <= 1'b0;
                end
            end
            else begin
                axis_tvalid_delay <= axis_tvalid;
            end
            axis_tlast_delay <= 1'b0;
        end
    end
    
    always @(posedge M_AXIS_ACLK) begin
        if (!M_AXIS_ARESETN) begin
            read_idx <= 1'b0;
            write_idx <= 1'b0;
            samples[0] <= 0;
            samples[1] <= 0;
            samples_cnt <= 0;
            stream_data_out <= 0;
            overflow <= 0;
        end
        else begin
            if (new_sample) begin
                if (samples_cnt >= 2) begin
                    overflow <= 1;
                end
                else begin
                    overflow <= 0;
                    samples[write_idx] <= sample;
                    write_idx <= write_idx + 1;
                    samples_cnt <= samples_cnt + 1;
                end
            end
            else begin
                if(tx_en && samples_cnt > 0) begin
                    /* Perform also sign extension */
                    stream_data_out[C_M_AXIS_TDATA_WIDTH-1 : 0] <= { {(C_M_AXIS_TDATA_WIDTH - 13){samples[read_idx][12]}}, samples[read_idx]};
                    read_idx <= read_idx + 1;
                    samples_cnt <= samples_cnt - 1;
                end
            end
        end
    end
endmodule
