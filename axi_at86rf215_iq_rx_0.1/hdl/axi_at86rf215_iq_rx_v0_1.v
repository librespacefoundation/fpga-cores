
`timescale 1 ns / 1 ps

	module axi_at86rf215_iq_rx_v0_1 #
	(
		// Users to add parameters here
        parameter integer SYNC_THRESHOLD = 5,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI_CFG
		parameter integer C_S00_AXI_CFG_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_CFG_ADDR_WIDTH	= 4,

		/* Data width of each IQ component */
		parameter integer C_M00_AXIS_IQ_TDATA_WIDTH	= 16
	)
	(
		// Users to add ports here
        input wire aclk,
        input wire iq_data,
        input wire resetn,
        output wire in_sync,
        output wire overflow,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI_CFG
		input wire  s00_axi_cfg_aclk,
		input wire [C_S00_AXI_CFG_ADDR_WIDTH-1 : 0] s00_axi_cfg_awaddr,
		input wire [2 : 0] s00_axi_cfg_awprot,
		input wire  s00_axi_cfg_awvalid,
		output wire  s00_axi_cfg_awready,
		input wire [C_S00_AXI_CFG_DATA_WIDTH-1 : 0] s00_axi_cfg_wdata,
		input wire [(C_S00_AXI_CFG_DATA_WIDTH/8)-1 : 0] s00_axi_cfg_wstrb,
		input wire  s00_axi_cfg_wvalid,
		output wire  s00_axi_cfg_wready,
		output wire [1 : 0] s00_axi_cfg_bresp,
		output wire  s00_axi_cfg_bvalid,
		input wire  s00_axi_cfg_bready,
		input wire [C_S00_AXI_CFG_ADDR_WIDTH-1 : 0] s00_axi_cfg_araddr,
		input wire [2 : 0] s00_axi_cfg_arprot,
		input wire  s00_axi_cfg_arvalid,
		output wire  s00_axi_cfg_arready,
		output wire [C_S00_AXI_CFG_DATA_WIDTH-1 : 0] s00_axi_cfg_rdata,
		output wire [1 : 0] s00_axi_cfg_rresp,
		output wire  s00_axi_cfg_rvalid,
		input wire  s00_axi_cfg_rready,

		// Ports of Axi Master Bus Interface M00_AXIS_I
		output wire  m00_axis_i_tvalid,
		output wire [C_M00_AXIS_IQ_TDATA_WIDTH-1 : 0] m00_axis_i_tdata,
		output wire [(C_M00_AXIS_IQ_TDATA_WIDTH/8)-1 : 0] m00_axis_i_tstrb,
		output wire  m00_axis_i_tlast,
		input wire  m00_axis_i_tready,

		// Ports of Axi Master Bus Interface M01_AXIS_Q
		output wire  m01_axis_q_tvalid,
		output wire [C_M00_AXIS_IQ_TDATA_WIDTH-1 : 0] m01_axis_q_tdata,
		output wire [(C_M00_AXIS_IQ_TDATA_WIDTH/8)-1 : 0] m01_axis_q_tstrb,
		output wire  m01_axis_q_tlast,
		input wire  m01_axis_q_tready
	);
	
	
	wire [3:0] samp_rate;
	wire [12:0] idata;
	wire [12:0] qdata;
	wire sample_valid;
	wire ioverflow;
	wire qoverflow;
	
	assign overflow = ioverflow | qoverflow;
	
// Instantiation of Axi Bus Interface S00_AXI_CFG
	axi_at86rf215_iq_rx_v0_1_S00_AXI_CFG # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_CFG_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_CFG_ADDR_WIDTH)
	) axi_at86rf215_iq_rx_v0_1_S00_AXI_CFG_inst (
		.samp_rate(samp_rate),
		.S_AXI_ACLK(s00_axi_cfg_aclk),
		.S_AXI_ARESETN(resetn),
		.S_AXI_AWADDR(s00_axi_cfg_awaddr),
		.S_AXI_AWPROT(s00_axi_cfg_awprot),
		.S_AXI_AWVALID(s00_axi_cfg_awvalid),
		.S_AXI_AWREADY(s00_axi_cfg_awready),
		.S_AXI_WDATA(s00_axi_cfg_wdata),
		.S_AXI_WSTRB(s00_axi_cfg_wstrb),
		.S_AXI_WVALID(s00_axi_cfg_wvalid),
		.S_AXI_WREADY(s00_axi_cfg_wready),
		.S_AXI_BRESP(s00_axi_cfg_bresp),
		.S_AXI_BVALID(s00_axi_cfg_bvalid),
		.S_AXI_BREADY(s00_axi_cfg_bready),
		.S_AXI_ARADDR(s00_axi_cfg_araddr),
		.S_AXI_ARPROT(s00_axi_cfg_arprot),
		.S_AXI_ARVALID(s00_axi_cfg_arvalid),
		.S_AXI_ARREADY(s00_axi_cfg_arready),
		.S_AXI_RDATA(s00_axi_cfg_rdata),
		.S_AXI_RRESP(s00_axi_cfg_rresp),
		.S_AXI_RVALID(s00_axi_cfg_rvalid),
		.S_AXI_RREADY(s00_axi_cfg_rready)
	);

    // I part of the IQ
	axi_at86rf215_iq_rx_M_AXI_S # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_IQ_TDATA_WIDTH)
	) axi_at86rf215_iq_rx_v0_1_M00_AXIS_I_inst (
	    .sample(idata),
        .new_sample(sample_valid),
        .overflow(ioverflow),
		.M_AXIS_ACLK(aclk),
		.M_AXIS_ARESETN(resetn),
		.M_AXIS_TVALID(m00_axis_i_tvalid),
		.M_AXIS_TDATA(m00_axis_i_tdata),
		.M_AXIS_TSTRB(m00_axis_i_tstrb),
		.M_AXIS_TLAST(m00_axis_i_tlast),
		.M_AXIS_TREADY(m00_axis_i_tready)
	);
	
	// Q part of the IQ
    axi_at86rf215_iq_rx_M_AXI_S # ( 
        .C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_IQ_TDATA_WIDTH)
    ) axi_at86rf215_iq_rx_v0_1_M01_AXIS_Q_inst (
        .sample(qdata),
        .new_sample(sample_valid),
        .overflow(qoverflow),
        .M_AXIS_ACLK(aclk),
        .M_AXIS_ARESETN(resetn),
        .M_AXIS_TVALID(m01_axis_q_tvalid),
        .M_AXIS_TDATA(m01_axis_q_tdata),
        .M_AXIS_TSTRB(m01_axis_q_tstrb),
        .M_AXIS_TLAST(m01_axis_q_tlast),
        .M_AXIS_TREADY(m01_axis_q_tready)
    );

	// Add user logic here
    at86rf215_iq_rx_sync # (
        .SYNC_THRESHOLD(SYNC_THRESHOLD)
    ) at86rf215_iq_rx_sync_inst(
        .iq_clk(aclk),
        .iq_data(iq_data),
        .samp_rate(samp_rate),
        .resetn(resetn),
        .idata(idata),
        .qdata(qdata),
        .valid(sample_valid),
        .in_sync(in_sync)
    );        
        
	// User logic ends

	endmodule
