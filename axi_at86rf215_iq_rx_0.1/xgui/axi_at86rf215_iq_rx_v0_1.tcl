# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "C_S00_AXI_CFG_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_CFG_HIGHADDR" -parent ${Page_0}

  set SYNC_THRESHOLD [ipgui::add_param $IPINST -name "SYNC_THRESHOLD"]
  set_property tooltip {Specifies the number of concecutive valid IQ 32-bit words that must be received, before the core indicates the samples as valid} ${SYNC_THRESHOLD}

}

proc update_PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH { PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to update C_M00_AXIS_IQ_TDATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH { PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to validate C_M00_AXIS_IQ_TDATA_WIDTH
	return true
}

proc update_PARAM_VALUE.SYNC_THRESHOLD { PARAM_VALUE.SYNC_THRESHOLD } {
	# Procedure called to update SYNC_THRESHOLD when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYNC_THRESHOLD { PARAM_VALUE.SYNC_THRESHOLD } {
	# Procedure called to validate SYNC_THRESHOLD
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_CFG_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_CFG_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_CFG_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_CFG_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_BASEADDR { PARAM_VALUE.C_S00_AXI_CFG_BASEADDR } {
	# Procedure called to update C_S00_AXI_CFG_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_BASEADDR { PARAM_VALUE.C_S00_AXI_CFG_BASEADDR } {
	# Procedure called to validate C_S00_AXI_CFG_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR { PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR } {
	# Procedure called to update C_S00_AXI_CFG_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR { PARAM_VALUE.C_S00_AXI_CFG_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_CFG_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CFG_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CFG_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.SYNC_THRESHOLD { MODELPARAM_VALUE.SYNC_THRESHOLD PARAM_VALUE.SYNC_THRESHOLD } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SYNC_THRESHOLD}] ${MODELPARAM_VALUE.SYNC_THRESHOLD}
}

proc update_MODELPARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH { MODELPARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH}] ${MODELPARAM_VALUE.C_M00_AXIS_IQ_TDATA_WIDTH}
}

